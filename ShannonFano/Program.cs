﻿using System;
using System.Collections.Generic;
using System.Linq;

class Symbol
{
    public char Character { get; set; }
    public int Frequency { get; set; }
    public string Code { get; set; }
}

class ShannonFano
{
    private List<Symbol> symbols;

    public ShannonFano()
    {
        symbols = new List<Symbol>();
    }

    public void AddSymbol(char c, int frequency)
    {
        symbols.Add(new Symbol { Character = c, Frequency = frequency });
    }

    public void Encode()
    {
        symbols = symbols.OrderByDescending(s => s.Frequency).ToList();
        EncodeSymbols(0, symbols.Count - 1);
    }

    private void EncodeSymbols(int startIndex, int endIndex)
    {
        if (startIndex == endIndex)
        {
            return;
        }

        int sum = symbols.GetRange(startIndex, endIndex - startIndex + 1).Sum(s => s.Frequency);

        int halfSum = 0;
        int i;
        for (i = startIndex; i <= endIndex; i++)
        {
            halfSum += symbols[i].Frequency;
            if (halfSum * 2 >= sum)
            {
                break;
            }
        }

        for (int j = startIndex; j <= endIndex; j++)
        {
            if (j <= i)
            {
                symbols[j].Code += "0";
            }
            else
            {
                symbols[j].Code += "1";
            }
        }

        EncodeSymbols(startIndex, i);
        EncodeSymbols(i + 1, endIndex);
    }

    public string EncodeMessage(string message)
    {
        string encodedMessage = "";
        foreach (char c in message)
        {
            Symbol symbol = symbols.Find(s => s.Character == c);
            if (symbol != null)
            {
                encodedMessage += symbol.Code;
            }
        }
        return encodedMessage;
    }

    public string DecodeMessage(string encodedMessage)
    {
        string decodedMessage = "";
        string currentCode = "";
        foreach (char c in encodedMessage)
        {
            currentCode += c;
            Symbol symbol = symbols.Find(s => s.Code == currentCode);
            if (symbol != null)
            {
                decodedMessage += symbol.Character;
                currentCode = "";
            }
        }
        return decodedMessage;
    }
}

class Program
{
    static void Main(string[] args)
    {
        ShannonFano sf = new ShannonFano();

        Console.Write("Введiть: ");
        string message = Console.ReadLine();

        // Визначення частоти кожного символу в повідомленні
        Dictionary<char, int> frequencies = new Dictionary<char, int>();
        foreach (char c in message)
        {
            if (frequencies.ContainsKey(c))
            {
                frequencies[c]++;
            }
            else
            {
                frequencies[c] = 1;
            }
        }


        // Додавання кожного символу та його частоти до кодера Шеннона-Фано
        foreach (KeyValuePair<char, int> pair in frequencies)
        {
            sf.AddSymbol(pair.Key, pair.Value);
        }

        sf.Encode();
        string encodedMessage = sf.EncodeMessage(message);
        string decodedMessage = sf.DecodeMessage(encodedMessage);

        Console.WriteLine("Введене повiдомлення: " + message);
        Console.WriteLine("Закодоване повiдомлення: " + encodedMessage);
        Console.WriteLine("Розкодоване повiдомлення: " + decodedMessage);
        Console.Read();
    }
}
